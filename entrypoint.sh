#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
# or first arg is `something.conf`
if [ "${1#-}" != "$1" ] || [ "${1%.conf}" != "$1" ]; then
    set -- node "$@"
fi

if [ ! -f /config/dev.yml ]; then
    echo "listen:" > /config/dev.yml
    echo "  port: $PEERTUBE_WEBSERVER_PORT" >> /config/dev.yml
fi

# export NOCLIENT=1
# exec gosu peertube "cd /app && yarn install --pure-lockfile && npm run setup:cli"

# allow the container to be started with `--user`
if [ "$1" = 'node' -a "$(id -u)" = '0' ]; then
    find /config ! -user peertube -exec chown peertube:peertube {} \; || true
    find /var/www/peertube/storage ! -user peertube -exec  chown peertube:peertube {} \;
    exec gosu peertube "$0" "$@"
fi

exec "$@"
