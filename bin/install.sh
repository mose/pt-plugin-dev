#!/bin/bash
set -euo pipefail

PEERTUBE_PATH=""


function install_instance() {
  instance=$1
  docker-compose exec -u peertube peertube$instance.localhost peertube plugins uninstall --npm-name 'peertube-plugin-livechat' --url "http://peertube$instance.localhost:900$instance" --username root --password 'test'
  docker-compose exec -u peertube peertube$instance.localhost peertube plugins install --path '/peertube-plugin-livechat' --url "http://peertube$instance.localhost:900$instance" --username root --password 'test'
  docker-compose exec -u peertube peertube$instance.localhost rm -rf /home/peertube/.cache/yarn
}

for instance in $*
do
  case $instance in
    1)
      install_instance 1;;
    2)
      install_instance 2;;
    go)
      $0 1 &
      $0 2 &
      ;;
    *)
      echo "paramètre inconnu";;
  esac
done
