# check https://hub.docker.com/r/chocobozzz/peertube/tags
# for chosing which versoin you want to run with
FROM chocobozzz/peertube:production-bullseye

RUN mkdir -p /var/www/peertube/storage
VOLUME /var/www/peertube/storage

COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]

# EXPOSE 9000 1935 

CMD ["node", "--inspect=0.0.0.0", "dist/server"]
